class Review < ApplicationRecord
    belongs_to :movie
    belongs_to :moviegoer
    validates :movie_id, :presence => true
    validates_associated :movie
    validates_associated :moviegoer
    
end
