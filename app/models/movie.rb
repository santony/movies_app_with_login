class Movie < ApplicationRecord
    has_many :reviews
    scope :for_kids, -> {where(rating: 'PG')}
end
